﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace IQTwist
{

public partial class IQTwistForm : Form
{    
    private const string DLL_NAME = "IQTwist_engine.dll";
        
    private static readonly Color BLUE = Color.FromArgb(0, 80, 240);
    private static readonly Color YELLOW = Color.FromArgb(230, 220, 40);
    private static readonly Color RED = Color.FromArgb(230, 50, 30);
    private static readonly Color GREEN = Color.FromArgb(30, 230, 60);
    
    private static readonly Color BLUE2 = Color.FromArgb(0, 60, 170);
    private static readonly Color YELLOW2 = Color.FromArgb(200, 170, 0);
    private static readonly Color RED2 = Color.FromArgb(170, 0, 60);
    private static readonly Color GREEN2 = Color.FromArgb(0, 170, 30);

    private static readonly Color BLACK = Color.FromArgb(0, 0, 0);
    private static readonly Color GRAY = Color.FromArgb(120, 120, 120);

    private Color[] m_colors = new Color[8] { BLUE, YELLOW, RED, GREEN, BLUE2, YELLOW2, RED2, GREEN2 };    

    class Square: PictureBox
    {        
        public int m_color;
        public Square()
        {
            m_color = 4;
            BackColor = BLACK;            
        }
    }
    
    private const int SQUARE_SIZE = 40;             
    private PictureBox[] m_tiles = new PictureBox[32];
    private PictureBox[] m_holes = new PictureBox[32];
    private Square[] m_pegs = new Square[32];

    System.Diagnostics.Stopwatch m_stopwatch = new System.Diagnostics.Stopwatch();

    [DllImport(DLL_NAME)]
    private static extern void initDLL();

    [DllImport(DLL_NAME)]
    private static extern void getSolution(int[] pegs, int[] solution);

    public IQTwistForm()
    {
        InitializeComponent();
        drawBoard();
        pnlBoard.SendToBack();
        pnlBoard.BackColor = GRAY;
        initDLL();
    }

    private void drawBoard()
    {
        const int X = 4;
        const int Y = 136;        
        int i = 0;
        for (int y = 0; y < 4; y++)
        {
            for (int x = 0; x < 8; x++, i++)
            {
                m_tiles[i] = new Square();
                m_tiles[i].Size = new Size(SQUARE_SIZE, SQUARE_SIZE);
                m_tiles[i].Location = new Point(X + x*(SQUARE_SIZE+4), Y + y*-(SQUARE_SIZE+4));                
                pnlBoard.Controls.Add(m_tiles[i]);

                m_holes[i] = new Square();
                m_holes[i].Size = new Size(SQUARE_SIZE-14, SQUARE_SIZE-14);
                m_holes[i].Location = new Point(X + x*(SQUARE_SIZE+4) + 7, Y + y*-(SQUARE_SIZE+4) + 7);
                pnlBoard.Controls.Add(m_holes[i]);

                m_pegs[i] = new Square();
                m_pegs[i].Size = new Size(SQUARE_SIZE-26, SQUARE_SIZE-26);
                m_pegs[i].Location = new Point(X + x*(SQUARE_SIZE+4) + 13, Y + y*-(SQUARE_SIZE+4) + 13);
                m_pegs[i].Click += new EventHandler(square_Click);
                pnlBoard.Controls.Add(m_pegs[i]);
            }
        }
        btn_ClearClick(null, null);
    }

    private void square_Click(object sender, EventArgs e)
    {
        Square square = (Square)sender;
        if (((MouseEventArgs)e).Button == MouseButtons.Right)
        {
            square.m_color = 4;
            square.BackColor = BLACK;
        }
        else if (((MouseEventArgs)e).Button == MouseButtons.Left)
        {            
            square.m_color++;
            if (square.m_color > 3)
                square.m_color = 0;
            square.BackColor = m_colors[square.m_color];
        }
    }

    private void btn_ClearClick(object sender, EventArgs e)
    {
        for (int i = 0; i < 32; i++)
        {
            m_tiles[i].BackColor = BLACK;
            m_holes[i].BackColor = BLACK;
            m_pegs[i].BackColor = BLACK;
            m_pegs[i].m_color = 4;
            m_pegs[i].BringToFront();
        }
        lblNoResult.Visible = false;
        pnlBoard.Enabled = true;
        btnSolve.Enabled = true;
        pnlBoard.BackColor = GRAY;           
    }

    private void btn_SolveClick(object sender, EventArgs e)
    {        
        lblWait.Visible = true;
        lblWait.Update();
        pnlBoard.Enabled = false;
        btnSolve.Enabled = false;
        btnClear.Enabled = false;
        int[] pegs = new int[4] { 0, 0, 0, 0 };
        for (int color = 0; color < 4; color++)
        {
            for (int i = 0; i < 32; i++)
            {
                if (m_pegs[i].m_color == color)
                    pegs[color] |= 1 << i;
            }
        }

        int[] solutionSet = new int[16];
        m_stopwatch.Start();
        getSolution(pegs, solutionSet);
        m_stopwatch.Stop();
        Console.WriteLine("Elapsed = {0}", m_stopwatch.Elapsed);
        m_stopwatch.Reset();                
        if (0 == solutionSet[0])
        {
            lblNoResult.Visible = true;
            pnlBoard.BackColor = RED;
        }
        else
        {
            renderBlocks(solutionSet);
        }

        lblWait.Visible = false;            
        btnClear.Enabled = true;
    }

    private void renderBlocks(int[] solutionSet)
    {
        for (int type = 0; type < 8; type++)
        {
            for (int i = 0; i < 32; i++)
            {
                int j = type * 2;
                if ((solutionSet[j] >> i & 1) != 0)
                {                    
                    m_tiles[i].BackColor = m_colors[type];
                    m_holes[i].SendToBack();
                }
                else if ((solutionSet[j+1] >> i & 1) != 0)
                {
                    m_tiles[i].BackColor = m_colors[type];
                    m_holes[i].BringToFront();
                }
            }
        }  

        for (int i = 0; i < 32; i++)
        {
            if (m_pegs[i].BackColor != BLACK)
                m_pegs[i].BringToFront();
            else
                m_pegs[i].SendToBack();
        }
        pnlBoard.SendToBack();        
    }

}
}
