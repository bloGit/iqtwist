﻿namespace IQTwist
{
    partial class IQTwistForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSolve = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlBoard = new System.Windows.Forms.Panel();
            this.lblManual = new System.Windows.Forms.Label();
            this.lblManual2 = new System.Windows.Forms.Label();
            this.lblNoResult = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSolve
            // 
            this.btnSolve.Location = new System.Drawing.Point(155, 279);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(111, 23);
            this.btnSolve.TabIndex = 0;
            this.btnSolve.Text = "Find solution";
            this.btnSolve.UseVisualStyleBackColor = true;
            this.btnSolve.Click += new System.EventHandler(this.btn_SolveClick);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(40, 279);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(72, 23);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btn_ClearClick);
            // 
            // pnlBoard
            // 
            this.pnlBoard.BackColor = System.Drawing.Color.Gray;
            this.pnlBoard.Location = new System.Drawing.Point(31, 79);
            this.pnlBoard.Name = "pnlBoard";
            this.pnlBoard.Size = new System.Drawing.Size(356, 180);
            this.pnlBoard.TabIndex = 2;
            // 
            // lblManual
            // 
            this.lblManual.AutoSize = true;
            this.lblManual.Location = new System.Drawing.Point(28, 15);
            this.lblManual.Name = "lblManual";
            this.lblManual.Size = new System.Drawing.Size(365, 13);
            this.lblManual.TabIndex = 3;
            this.lblManual.Text = "To put a peg click in the middle of a square. To change its color click again.";
            // 
            // lblManual2
            // 
            this.lblManual2.AutoSize = true;
            this.lblManual2.Location = new System.Drawing.Point(128, 32);
            this.lblManual2.Name = "lblManual2";
            this.lblManual2.Size = new System.Drawing.Size(165, 13);
            this.lblManual2.TabIndex = 4;
            this.lblManual2.Text = "To remove a peg right-click on it. ";
            // 
            // lblNoResult
            // 
            this.lblNoResult.AutoSize = true;
            this.lblNoResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNoResult.ForeColor = System.Drawing.Color.Red;
            this.lblNoResult.Location = new System.Drawing.Point(98, 60);
            this.lblNoResult.Name = "lblNoResult";
            this.lblNoResult.Size = new System.Drawing.Size(227, 13);
            this.lblNoResult.TabIndex = 5;
            this.lblNoResult.Text = "A solution for these pegs doesn\'t exist.";
            this.lblNoResult.Visible = false;
            // 
            // lblWait
            // 
            this.lblWait.AutoSize = true;
            this.lblWait.Location = new System.Drawing.Point(295, 283);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(70, 13);
            this.lblWait.TabIndex = 6;
            this.lblWait.Text = "Please wait...";
            this.lblWait.Visible = false;
            // 
            // IQTwistForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 315);
            this.Controls.Add(this.lblWait);
            this.Controls.Add(this.lblNoResult);
            this.Controls.Add(this.lblManual2);
            this.Controls.Add(this.lblManual);
            this.Controls.Add(this.pnlBoard);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSolve);
            this.Name = "IQTwistForm";
            this.Text = "IQ Twist Solution Finder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSolve;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel pnlBoard;
        private System.Windows.Forms.Label lblManual;
        private System.Windows.Forms.Label lblManual2;
        private System.Windows.Forms.Label lblNoResult;
        private System.Windows.Forms.Label lblWait;
    }
}

