#pragma once
#include <mutex>
#include <array>
#include "ShapeVariant.h"

class IQTwistResolver
{            
    enum EShapeType //enum order is arranged so that shape-type can be mapped directly to color with % 4
    {
        BLUE_4      = 0,
        YELLOW_5    = 1,
        RED_L       = 2,
        GREEN_4     = 3,
        BLUE_5      = 4,
        YELLOW_3    = 5,
        RED_S       = 6,
        GREEN_3     = 7,
        TYPES_COUNT = 8
    };

    enum EColor
    {
        BLUE         = 0,
        YELLOW       = 1,
        RED          = 2,
        GREEN        = 3,
        ALL_COLORS   = 4,
        COLORS_COUNT = 5
    };

    enum ERotation
    {
        ___0_DEGREES    = 0,
        __90_DEGREES    = 1,
        _180_DEGREES    = 2,
        _270_DEGREES    = 3,
        ROTATIONS_COUNT = 4
    };

    public:

    IQTwistResolver();
    void findSolution(uint32 pins[ALL_COLORS], ShapeVariant solution[TYPES_COUNT]);


    private:    

    void assignBasicPatterns();
    void initializeShapes();

    //designate most-equal N ranges of indices to be searched by N threads
    void calculateRangesForThreads();

    //returns true if any shape's "tile" (closed subsegment) overlaps with a peg - or - any shape's "hole"
    //(open subsegment) overlaps with a peg which has a different color (holes CAN overlap with pegs of the same color);
    bool isPegCollision(const ShapeVariant variant, const EColor color) const;

    //recursive function which searches the whole tree of possible shapes combinations
    bool searchForSolution(const int type, const uint32 a_union);

    //data members:

    static const int MAX_VARIANTS = 168; //max variants per shape type
    static const int MAX_THREADS = 16;

    //basic patterns representing 8 distinct shapes (shape-types) of all 4 rotations,
    //they are 'seeds' for the initialization of all possible variants;
    std::array< std::array<ShapeVariant, ROTATIONS_COUNT>, TYPES_COUNT> m_patterns;

    //all possible variants - rotations, flipped (mirrored) rotations and all locations on the board - of all the shape-types;
    std::array< std::array<ShapeVariant, MAX_VARIANTS>, TYPES_COUNT> m_shapes;
    
    //pegs or 'pins' which are provided by GUI and impose the required layout of the shapes,
    //last element (ALL_COLORS) is for merged (bitwise OR'ed) all 4 colors together;
    std::array<uint32, COLORS_COUNT> m_pegs;
    
    //a buffer for the solution - the target variants of all 8 shape-types (if exist, otherwise - all zeroes);
    std::array<ShapeVariant, TYPES_COUNT> m_solutionSet;

    //a flag set by a thread which has found the solution (finder thread) to make other threads stop searching;
    bool m_stopSearch {false};

    //on the root level of the search (shape type 0) the loop with variants is split among threads;
    //ranges of the split loop are determined dynamically at initialization step
    //this array stores boundary indices of the ranges which are 'assigned' to thraeds accordingly;
    std::array<int, MAX_THREADS + 1> m_indexRanges;
    
    int m_threadsCount {0};
    std::array<std::thread, MAX_THREADS> m_threads;

    //mutex which must guard an exclusive access to m_stopSearch resolution block;
    std::mutex m_mutex;
};

