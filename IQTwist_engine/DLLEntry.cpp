#include <memory>
#include "IQTwistResolver.h"

namespace
{
    std::unique_ptr<IQTwistResolver> pResolver;
}

extern "C" __declspec(dllexport) void initDLL()
{
    pResolver = std::make_unique<IQTwistResolver>();
}

extern "C" __declspec(dllexport) void getSolution(uint32 pegs[4], ShapeVariant solution[8])
{
    if (pResolver != nullptr)
    {
        pResolver->findSolution(pegs, solution);
    }
}
