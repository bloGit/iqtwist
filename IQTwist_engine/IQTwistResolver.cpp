#include <thread>
#include <algorithm> //std::copy, std::fill
#include "IQTwistResolver.h"

IQTwistResolver::IQTwistResolver()
{
    assignBasicPatterns();
    initializeShapes();
    calculateRangesForThreads();
}

void IQTwistResolver::findSolution(uint32 pegs[ALL_COLORS], ShapeVariant solution[TYPES_COUNT])
{
    std::copy(pegs,  pegs + ALL_COLORS, m_pegs.begin());
    m_pegs[ALL_COLORS] = m_pegs[BLUE] | m_pegs[YELLOW] | m_pegs[RED] | m_pegs[GREEN];
    
    std::fill(m_solutionSet.begin(), m_solutionSet.end(), ShapeVariant(0, 0)); //reset all to 0
        
    m_stopSearch = false;    
    for (int t {0}; t < m_threadsCount; t++)
    {        
        m_threads[t] = std::thread( [this, t] //each thread runs lambda to iterate over the first tree level (shape-type index 0)
        {
            for (int i {m_indexRanges[t]}; i < m_indexRanges[t + 1]; i++) //iterate over a thread-assigned range of variants
            {
                ShapeVariant variant {m_shapes[0][i]};
                if ( !isPegCollision(variant, static_cast<EColor>(0)) && searchForSolution(1, variant.full()) )
                {
                    m_solutionSet[0] = variant;                 
                    break;
                }
            }
        });
    }

    for (int t {0}; t < m_threadsCount; t++)
    {
        m_threads[t].join();
    }

    std::copy(m_solutionSet.begin(), m_solutionSet.end(), solution);    
}

void IQTwistResolver::assignBasicPatterns()
{
    m_patterns[BLUE_4][___0_DEGREES]   = ShapeVariant(0x01010001, 0x00000100);
    m_patterns[BLUE_4][__90_DEGREES]   = ShapeVariant(0x0000000d, 0x00000002);
    m_patterns[BLUE_4][_180_DEGREES]   = ShapeVariant(0x0000000b, 0x00000004);

    m_patterns[YELLOW_5][___0_DEGREES] = ShapeVariant(0x00000300, 0x00040402);
    m_patterns[YELLOW_5][__90_DEGREES] = ShapeVariant(0x00020200, 0x00000106);
    m_patterns[YELLOW_5][_180_DEGREES] = ShapeVariant(0x00000600, 0x00020101);
    m_patterns[YELLOW_5][_270_DEGREES] = ShapeVariant(0x00000202, 0x00030400);

    m_patterns[RED_L][___0_DEGREES]    = ShapeVariant(0x00000102, 0x00010001);
    m_patterns[RED_L][__90_DEGREES]    = ShapeVariant(0x00000201, 0x00000500);
    m_patterns[RED_L][_180_DEGREES]    = ShapeVariant(0x00010200, 0x00020002);
    m_patterns[RED_L][_270_DEGREES]    = ShapeVariant(0x00000402, 0x00000005);

    m_patterns[GREEN_4][___0_DEGREES]  = ShapeVariant(0x00000101, 0x00010200);
    m_patterns[GREEN_4][__90_DEGREES]  = ShapeVariant(0x00000300, 0x00000402);
    m_patterns[GREEN_4][_180_DEGREES]  = ShapeVariant(0x00020200, 0x00000102);
    m_patterns[GREEN_4][_270_DEGREES]  = ShapeVariant(0x00000006, 0x00000201);

    m_patterns[BLUE_5][___0_DEGREES]   = ShapeVariant(0x00010101, 0x00020200);
    m_patterns[BLUE_5][__90_DEGREES]   = ShapeVariant(0x00000700, 0x00000006);
    m_patterns[BLUE_5][_180_DEGREES]   = ShapeVariant(0x00020202, 0x00000101);
    m_patterns[BLUE_5][_270_DEGREES]   = ShapeVariant(0x00000007, 0x00000300);

    m_patterns[YELLOW_3][___0_DEGREES] = ShapeVariant(0x00000101, 0x00010000);
    m_patterns[YELLOW_3][__90_DEGREES] = ShapeVariant(0x00000003, 0x00000004);
    m_patterns[YELLOW_3][_180_DEGREES] = ShapeVariant(0x00000006, 0x00000001);

    m_patterns[RED_S][___0_DEGREES]    = ShapeVariant(0x00010202, 0x00000100);
    m_patterns[RED_S][__90_DEGREES]    = ShapeVariant(0x00000403, 0x00000200);
    m_patterns[RED_S][_180_DEGREES]    = ShapeVariant(0x00010102, 0x00000200);
    m_patterns[RED_S][_270_DEGREES]    = ShapeVariant(0x00000601, 0x00000002);

    m_patterns[GREEN_3][___0_DEGREES]  = ShapeVariant(0x00000100, 0x00000003);
    m_patterns[GREEN_3][__90_DEGREES]  = ShapeVariant(0x00000200, 0x00000101);
    m_patterns[GREEN_3][_180_DEGREES]  = ShapeVariant(0x00000002, 0x00000300);
    m_patterns[GREEN_3][_270_DEGREES]  = ShapeVariant(0x00000001, 0x00000202);
}

void IQTwistResolver::initializeShapes()
{    
    for (int type {0}; type < TYPES_COUNT; type++)
    {    
        int variant {0};
        for (int rotation {0}; rotation < ROTATIONS_COUNT; rotation++)
        {        
            ShapeVariant pattern {m_patterns[type][rotation]};
            const int width {pattern.width()};
            const int height {pattern.height()};
            for (int x {0}; width > 0 && x <= ShapeVariant::BOARD_WIDTH - width; x++) //assumes shapes' bits are 'left-bottom' aligned
            {
                for (int y {0}; y <= ShapeVariant::BOARD_HEIGHT - height; y++, variant++)
                {
                    m_shapes[type][variant] = pattern;
                    m_shapes[type][variant].shift(x, y);
                }
            }
            pattern.mirror(); //vertical mirror makes no sense for height == 1, avoid generating them
            for (int x {0}; height > 1 && x <= ShapeVariant::BOARD_WIDTH - width; x++)
            {
                for (int y {0}; y <= ShapeVariant::BOARD_HEIGHT - height; y++, variant++)
                {
                    m_shapes[type][variant] = pattern;
                    m_shapes[type][variant].shift(x, y);
                }
            }
        }
    }
}

void IQTwistResolver::calculateRangesForThreads()
{
    m_threadsCount = std::thread::hardware_concurrency();
    if (0 == m_threadsCount || m_threadsCount > MAX_THREADS)
    {
        m_threadsCount = MAX_THREADS;
    }

    int type0_variantCount {0};
    while (type0_variantCount < MAX_VARIANTS && m_shapes[0][type0_variantCount].full() != 0)
    {
        type0_variantCount++;
    }
    const int minLenght {type0_variantCount / m_threadsCount};
    const int remainder {type0_variantCount % m_threadsCount};

    int index {0};
    for (int i {0}; i <= m_threadsCount; i++)
    {
        m_indexRanges[i] = index;
        index += minLenght + (i < remainder ? 1 : 0); //distribute the remainder most evenly (+1 to each subsequent range)
    }
}

bool IQTwistResolver::isPegCollision(const ShapeVariant variant, const EColor a_color) const
{
    if ((variant.tiles() & m_pegs[ALL_COLORS]) != 0) //if tiles collide (overlap) with any pegs
        return true;

    for (int color {0}; color < ALL_COLORS; color++)
    {
        if ((variant.holes() & m_pegs[color]) != 0 && color != a_color) //if holes meet certain pegs but it's not the same color
            return true;
    }
    return false; //neither tiles collide nor wrong color's holes - there is no collision
}

bool IQTwistResolver::searchForSolution(const int type, const uint32 a_union)
{
    if (m_stopSearch) //make all other threads unwind recursion and finish
        return false;    

    if (TYPES_COUNT == type) //all types reached and solved - end recursion
    {
        //often a few solutions exist, in theory it may happen that more than one thread finds a solution and gets in here
        //at the same time, we need to protect the 'm_solutionSet' buffer from overwriting by another thread during unwinding        
        std::lock_guard<std::mutex> lock(m_mutex);
        if (!m_stopSearch)
        {            
            m_stopSearch = true;
            return true; //return true to collect the solution and unwind
        }
        return false; //make "late" solving thread return false to skip solution collection
    }

    for (int i {0}; i < MAX_VARIANTS && m_shapes[type][i].full() != 0; i++) //iterate over all variants of this shape type
    {
        ShapeVariant variant {m_shapes[type][i]};
        if ( 0 == (variant.full() & a_union) && !isPegCollision(variant, static_cast<EColor>(type % ALL_COLORS)) )
        {
            //if this variant doesn't overlap with up-to-now collected variants from the searched path (a union)
            //and doesn't collide with pegs - start searching for the next (deeper) type
            if ( searchForSolution(type + 1, variant.full() | a_union) ) //pass type + 1 and fresh (OR'ed) union to deeper level
            {                                                    
                m_solutionSet[type] = variant;
                return true;
            }
        }
    }
    return false; //no non-overlaping variant found - tell previous type to try its another variant...
}

