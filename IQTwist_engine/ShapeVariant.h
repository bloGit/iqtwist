#pragma once

using uint32 = unsigned long;

class ShapeVariant
{
    public:
    
    //public constants:
    static const int BOARD_WIDTH = 8;
    static const int BOARD_HEIGHT = 4;


    //methods:
    ShapeVariant();    
    ShapeVariant(uint32 tiles, uint32 holes);    

    uint32 tiles() const;
    uint32 holes() const;
    uint32 full() const;

    int width() const;
    int height() const;    

    void shift(int x, int y);
    void mirror();    

    void printShape() const;


    private:
    
    //data members:
    static const uint32 BOARD_LEFT_HALF {0x0f0f0f0f};

    uint32 m_tiles {0};
    uint32 m_holes {0};
};

inline uint32 ShapeVariant::tiles() const
{
    return m_tiles;
}

inline uint32 ShapeVariant::holes() const
{
    return m_holes;
}

inline uint32 ShapeVariant::full() const
{
    return m_tiles | m_holes;
}
