#include <iostream> //std::cout
#include <intrin.h> //_byteswap_ulong
#include "ShapeVariant.h"

ShapeVariant::ShapeVariant()
{ }

ShapeVariant::ShapeVariant(uint32 tiles, uint32 holes): m_tiles{tiles}, m_holes{holes}
{ }

int ShapeVariant::width() const //assumes that shape's bits are 'left-bottom' aligned
{
    uint32 shape {full()};
    int width {0};
    while ((shape & BOARD_LEFT_HALF) != 0) //& BOARD_LEFT_HALF - reset bits which wrap (overflow) to the other side
    {
        width++;
        shape >>= 1;
    }
    return width;
}

int ShapeVariant::height() const //assumes that shape's bits are 'left-bottom' aligned
{
    uint32 shape {full()};
    int height {0};
    while (shape != 0)
    {
        height++;
        shape >>= BOARD_WIDTH;
    }
    return height;
}

void ShapeVariant::shift(int x, int y)
{
    m_tiles <<= x;
    m_holes <<= x;
    m_tiles <<= y * BOARD_WIDTH;
    m_holes <<= y * BOARD_WIDTH;
}

void ShapeVariant::mirror()
{
    if (full() != 0) //prevent endless 'while' loop in case the variant is 0
    {
        //flip bits vertically in this variant        
        m_tiles = _byteswap_ulong(m_tiles); //since the board width is 8 ( == bits num in byte) let byteswap do the job
        m_holes = _byteswap_ulong(m_holes);

        //because flipped bits occupy most significant ('top') bytes align the variant back to 'bottom'
        while (0 == (full() & 0xFF))
        {        
            m_tiles >>= BOARD_WIDTH;
            m_holes >>= BOARD_WIDTH;
        }
    }
}

void ShapeVariant::printShape() const //debug purpose only
{
    for (int y {BOARD_HEIGHT - 1}; y >= 0; y--)
    {
        const uint32 tilesRow {m_tiles >> y * BOARD_WIDTH};
        const uint32 holesRow {m_holes >> y * BOARD_WIDTH};
        for (int x {0}; x < BOARD_WIDTH; x++)
        {
            if ((tilesRow >> x & 1) != 0 && (holesRow >> x & 1) != 0)
            {
                throw "tiles-holes overlap";
            }

            if ((tilesRow >> x & 1) != 0)
                std::cout << "1" << " ";
            else if ((holesRow >> x & 1) != 0)
                std::cout << "0" << " ";
            else
                std::cout << "." << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}